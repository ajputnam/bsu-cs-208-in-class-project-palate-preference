const express = require('express');
const app = express();
const path = require('path');

app.set('view engine', 'pug');
app.use(express.static("public"));

app.get('/', function(req, res){
    res.render("index", {'name': 'User'})
});

app.get('/json', function(req, res){
    res.json({key:"value"});
});

app.use( (req, res) => {
    res.status(404).render('404')
})

app.listen(3000);